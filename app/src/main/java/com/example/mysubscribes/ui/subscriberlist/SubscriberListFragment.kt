package com.example.mysubscribes.ui.subscriberlist

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.mysubscribes.R
import com.example.mysubscribes.data.db.AppDatabase
import com.example.mysubscribes.data.db.dao.SubscriberDao
import com.example.mysubscribes.data.repository.DatabaseDataSource
import com.example.mysubscribes.data.repository.SubscriberRepository
import com.example.mysubscribes.extention.navigateWithAnimations
import com.example.mysubscribes.ui.subscriber.SubscriberViewModel
import kotlinx.android.synthetic.main.subscriber_list_fragment.*

class SubscriberListFragment : Fragment(R.layout.subscriber_list_fragment) {

    @Suppress("UNCHECKED_CAST")
    private val viewModel: SubscriberListViewModel by viewModels {
        object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                val subscriberDao: SubscriberDao =
                    AppDatabase.getInstance(requireContext()).subscriberDao
                val repository: SubscriberRepository = DatabaseDataSource(subscriberDao)
                return SubscriberListViewModel(repository) as T
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observerViewModelEvents()
        configureViewListeners()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getSubscribers()
    }

    private fun observerViewModelEvents() {
        viewModel.allSubscriberEvent.observe(viewLifecycleOwner, { allSubscribers ->
            val subscriberListAdapter = SubscriberListAdapter(allSubscribers)
            with(recycler_subscribers) {
                setHasFixedSize(true)
                adapter = subscriberListAdapter
            }
        })

    }

    private fun configureViewListeners() {
        fabAddSubscriber.setOnClickListener {
            findNavController().navigateWithAnimations(R.id.subscriberFragment)
        }
    }

}